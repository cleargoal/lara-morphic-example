<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectHasContent extends Model
{
    use HasFactory;

    public function content()
    {
        return $this->morphTo();
    }
}
