<?php

namespace App\Http\Controllers;

use App\Models\ProjectArticle;
use Illuminate\Http\Request;

class ProjectArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $projectArticle = ProjectArticle::find(1);
        $projectArticle
            ->addMediaFromDisk('')
            ->toMediaCollection();

    }

}
